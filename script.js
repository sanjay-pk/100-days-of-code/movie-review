const formInput = document.getElementById('form');
const titleInput = document.getElementById('titleInput');
const descriptionInput = document.getElementById('descriptionInput');
const reviewDiv = document.getElementById('reviewDiv')



// adding submit event
formInput.addEventListener('submit',(event)=>{
    event.preventDefault()
console.log("submitttttt")
    const title = titleInput.value
    const description = descriptionInput.value

// createing article element
   const article = document.createElement('article')
   article.classList.add('bg-white','border', 'rounded', 'overflow-hidden' , 'p-3' , 'mt-1')

// creating and append h3 element

   const h3 = document.createElement('h3')
   h3.innerHTML = title
   article.appendChild(h3)

// creating and append p element

   const p = document.createElement('p')
   p.innerHTML = description
   article.appendChild(p)

// append article to reviewDiv

    reviewDiv.appendChild(article)

// after submition input and description in to empty

    document.getElementById("titleInput").value = "";
    document.getElementById("descriptionInput").value = "";

    console.log(article)
})